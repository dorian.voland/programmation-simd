make mjpeg-float 
make mjpeg-int 
make mjpeg-mmx 
make mjpeg-conv-unrolled4-float-a-trou 
make mjpeg-conv-sse-a-trou 

echo "" > res
{ time ./mjpeg-float ice_age_256x144_444.mjpeg -1 zyva } >> res
{ time ./mjpeg-int   ice_age_256x144_444.mjpeg -1 zyva } >> res
{ time ./mjpeg-mmx   ice_age_256x144_444.mjpeg -1 zyva } >> res
{ time ./mjpeg-conv-unrolled4-float-a-trou  ice_age_256x144_444.mjpeg -1 zyva } >> res
{ time ./mjpeg-conv-sse-a-trou   ice_age_256x144_444.mjpeg -1 zyva } >> res