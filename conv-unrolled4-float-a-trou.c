/*******************************************************************************
 * vim:set ts=3:
 * File   : conv.c, file for JPEG-JFIF sequential decoder    
 *
 * Copyright (C) 2007-2018 TIMA Laboratory
 * Author: Frédéric Pétrot <Frederic.Petrot@imag.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 *************************************************************************************/

#include "conv.h"
#include "stdlib.h"
#include "stdio.h"
#include "utils.h"

/*
 * TODO: Implantation déroulant la boucle 4 fois
 */
void YCrCb_to_ARGB(uint8_t *YCrCb_MCU[3], uint32_t *RGB_MCU, uint32_t nb_MCU_H, uint32_t nb_MCU_V)
{
   uint8_t *MCU_Y, *MCU_Cr, *MCU_Cb;
   uint8_t i, j;


   /* TODO: vos déclarations de variables ici */
   int32_t R0, G0, B0;
   int32_t R1, G1, B1;
   int32_t R2, G2, B2;
   int32_t R3, G3, B3;
   uint32_t ARGB0, ARGB1, ARGB2, ARGB3;
   uint8_t index0, index1, index2,index3;

   MCU_Y = YCrCb_MCU[0];
   MCU_Cr = YCrCb_MCU[2];
   MCU_Cb = YCrCb_MCU[1];

   for (i = 0; i < 8 * nb_MCU_V; i++) {
      for (j = 0; j < 8 * nb_MCU_H; j = j + 4) {
         /* On travaille à présent sur des vecteurs de 4 éléments d'un coup */
         /* TODO: juste fait le ! */
         index0= i * (8 * nb_MCU_H) + j + 0;
         index1= i * (8 * nb_MCU_H) + j + 1;
         index2= i * (8 * nb_MCU_H) + j + 2;
         index3= i * (8 * nb_MCU_H) + j + 3;

         R0 = (MCU_Cr[index0] - 128) * 1.402f + MCU_Y[index0];
         G0 = MCU_Y[index0] - (MCU_Cb[index0] - 128) * 0.381834f - (MCU_Cr[index0] - 128) * 0.71414f;
         B0 = (MCU_Cb[index0] - 128) * 1.7772f + MCU_Y[index0];
         /* Saturation des valeurs pour se ramener à 32 bits pour Alpha, Red, Green, Blue */
         if (R0 > 255) R0 = 255;
         if (R0 < 0)   R0 = 0;
         if (G0 > 255) G0 = 255;
         if (G0 < 0)   G0 = 0;
         if (B0 > 255) B0 = 255;
         if (B0 < 0)   B0 = 0;
         ARGB0 = ((R0 & 0xFF) << 16) | ((G0 & 0xFF) << 8) | (B0 & 0xFF);

         R1 = (MCU_Cr[index1] - 128) * 1.402f + MCU_Y[index1];
         G1 = MCU_Y[index1] - (MCU_Cb[index1] - 128) * 0.381834f - (MCU_Cr[index1] - 128) * 0.71414f;
         B1 = (MCU_Cb[index1] - 128) * 1.7772f + MCU_Y[index1];
         /* Saturation des valeurs pour se ramener à 32 bits pour Alpha, Red, Green, Blue */
         if (R1 > 255) R1 = 255;
         if (R1 < 0)   R1 = 0;
         if (G1 > 255) G1 = 255;
         if (G1 < 0)   G1 = 0;
         if (B1 > 255) B1 = 255;
         if (B1 < 0)   B1 = 0;
         ARGB1 = ((R1 & 0xFF) << 16) | ((G1 & 0xFF) << 8) | (B1 & 0xFF);

         R2 = (MCU_Cr[index2] - 128) * 1.402f + MCU_Y[index2];
         G2 = MCU_Y[index2] - (MCU_Cb[index2] - 128) * 0.381834f - (MCU_Cr[index2] - 128) * 0.71414f;
         B2 = (MCU_Cb[index2] - 128) * 1.7772f + MCU_Y[index2];
         /* Saturation des valeurs pour se ramener à 32 bits pour Alpha, Red, Green, Blue */
         if (R2 > 255) R2 = 255;
         if (R2 < 0)   R2 = 0;
         if (G2 > 255) G2 = 255;
         if (G2 < 0)   G2 = 0;
         if (B2 > 255) B2 = 255;
         if (B2 < 0)   B2 = 0;
         ARGB2 = ((R2 & 0xFF) << 16) | ((G2 & 0xFF) << 8) | (B2 & 0xFF);

         R3 = (MCU_Cr[index3] - 128) * 1.402f + MCU_Y[index3];
         G3 = MCU_Y[index3] - (MCU_Cb[index3] - 128) * 0.381834f - (MCU_Cr[index3] - 128) * 0.71414f;
         B3 = (MCU_Cb[index3] - 128) * 1.7772f + MCU_Y[index3];
         /* Saturation des valeurs pour se ramener à 32 bits pour Alpha, Red, Green, Blue */
         if (R3 > 255) R3 = 255;
         if (R3 < 0)   R3 = 0;
         if (G3 > 255) G3 = 255;
         if (G3 < 0)   G3 = 0;
         if (B3 > 255) B3 = 255;
         if (B3 < 0)   B3 = 0;
         ARGB3 = ((R3 & 0xFF) << 16) | ((G3 & 0xFF) << 8) | (B3 & 0xFF);

         /* Écriture du pixel dans le macro-bloc de sortie */
         RGB_MCU[(i * (8 * nb_MCU_H) + j + 0)] = ARGB0;
         RGB_MCU[(i * (8 * nb_MCU_H) + j + 1)] = ARGB1;
         RGB_MCU[(i * (8 * nb_MCU_H) + j + 2)] = ARGB2;
         RGB_MCU[(i * (8 * nb_MCU_H) + j + 3)] = ARGB3;
      }
   }
}
